package com.jklove.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.coupon.entity.CouponSpuCategoryRelationEntity;

import java.util.Map;

/**
 * 优惠券分类关联
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 11:45:47
 */
public interface CouponSpuCategoryRelationService extends IService<CouponSpuCategoryRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

