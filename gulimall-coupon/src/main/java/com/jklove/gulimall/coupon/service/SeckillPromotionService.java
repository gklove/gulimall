package com.jklove.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 11:45:47
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

