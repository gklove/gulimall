package com.jklove.gulimall.coupon.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jklove.gulimall.coupon.entity.CouponEntity;
import com.jklove.gulimall.coupon.service.CouponService;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.R;



/**
 * 优惠券信息
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 11:45:48
 */
@RefreshScope
@RestController
@RequestMapping("coupon/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    @Value("${coupon.from.config:aaa}")
    private String id;

    @RequestMapping("/feign/test")
    //@RequiresPermissions("coupon:coupon:list")
    public R getCouponList(){
        Map<String, Object> params = new HashMap<>();
        params.put("id", 1);
        PageUtils pageUtils = couponService.queryPage(params);
        return R.ok().put("coupon", pageUtils.getList());
    }
    @RequestMapping("/test")
    //@RequiresPermissions("coupon:coupon:list")
    public R configTest(){
        return R.ok().put("id", id);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("coupon:coupon:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = couponService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //RequiresPermissions@("coupon:coupon:info")
    public R info(@PathVariable("id") Long id){
		CouponEntity coupon = couponService.getById(id);

        return R.ok().put("coupon", coupon);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("coupon:coupon:save")
    public R save(@RequestBody CouponEntity coupon){
		couponService.save(coupon);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("coupon:coupon:update")
    public R update(@RequestBody CouponEntity coupon){
		couponService.updateById(coupon);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("coupon:coupon:delete")
    public R delete(@RequestBody Long[] ids){
		couponService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
