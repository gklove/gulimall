package com.jklove.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.product.entity.SpuImagesEntity;

import java.util.Map;

/**
 * spu图片
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

