package com.jklove.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jklove.common.constant.ProductConstant;
import com.jklove.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.jklove.gulimall.product.dao.AttrGroupDao;
import com.jklove.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.entity.CategoryEntity;
import com.jklove.gulimall.product.service.AttrGroupService;
import com.jklove.gulimall.product.vo.AttrEntityRespVo;
import com.jklove.gulimall.product.vo.AttrEntityVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.Query;

import com.jklove.gulimall.product.dao.AttrDao;
import com.jklove.gulimall.product.entity.AttrEntity;
import com.jklove.gulimall.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    private CategoryServiceImpl categoryService;

    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private AttrAttrgroupRelationDao relationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveAttr(AttrEntityVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.save(attrEntity);
        if (attr.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()&&attr.getAttrGroupId()!=null) {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            relationEntity.setAttrId(attrEntity.getAttrId());

            attrAttrgroupRelationDao.insert(relationEntity);
        }
    }

    @Override
    public PageUtils    baseAttrlist(Map<String, Object> params, Long categoryId,String attrType) {
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("attr_type", "base".equals(attrType) ? 1 : 0);//销售属性和基本属性的查询
        if (categoryId != 0) {
            wrapper.eq("catelog_id", categoryId);
        }

        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            wrapper.and(((w)->{
                w.eq("attr_id", key).or().like("attr_name", key);
            }));
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);
        List<AttrEntity> records = page.getRecords();
        List<AttrEntityRespVo> list = records.stream().map((r) -> {
            AttrEntityRespVo respVo = new AttrEntityRespVo();
            BeanUtils.copyProperties(r, respVo);
            CategoryEntity categoryEntity = categoryService.getById(r.getCatelogId());
            if (categoryEntity != null) {
                respVo.setCatelogName(categoryEntity.getName());
            }

           if ("base".equalsIgnoreCase(attrType)){
               AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao
                       .selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id",r.getAttrId()));
               if (attrAttrgroupRelationEntity != null&&attrAttrgroupRelationEntity.getAttrGroupId()!=null) {
                   AttrGroupEntity attrGroupEntity = attrGroupService.getById(attrAttrgroupRelationEntity.getAttrGroupId());
                   if (attrGroupEntity != null) {
                       respVo.setGroupName(attrGroupEntity.getAttrGroupName());
                   }

               }
           }

            return respVo;
        }).collect(Collectors.toList());
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(list);
        return pageUtils;
    }

    @Override
    public AttrEntityRespVo getAttr(Long attrId) {
        AttrEntity attrEntity = this.getById(attrId);
        AttrEntityRespVo resp = new AttrEntityRespVo();
        BeanUtils.copyProperties(attrEntity, resp);
        Long catelogId = attrEntity.getCatelogId();
        Long[] path = categoryService.getcatelogPath(catelogId);
        resp.setCatelogPath(path);
     if (attrEntity.getAttrType()==ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()){
         AttrAttrgroupRelationEntity relationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrId));
         if (relationEntity != null) {
             resp.setAttrGroupId(relationEntity.getAttrGroupId());
         }
     }
        return resp;
    }

    @Transactional
    @Override
    public void updateAttr(AttrEntityVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.updateById(attrEntity);
        if (attr.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {
            Long count = attrAttrgroupRelationDao.selectCount(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attr.getAttrId()));
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(attr.getAttrId());
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            if (count == 0) {
                attrAttrgroupRelationDao.insert(relationEntity);
            } else {
                attrAttrgroupRelationDao.update(relationEntity,new UpdateWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attr.getAttrId()));
            }
        }
    }
    @Override
    public List<AttrEntity>  getAttrRelation(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> relationEntityList = relationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_group_id", attrgroupId));
        List<Long> attrIds = relationEntityList.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(attrIds)) {
            return null;
        }
        return this.listByIds(attrIds);
    }

    @Override
    public PageUtils getNoAttrRelation(Long attrgroupId, Map<String, Object> params) {
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupId);
        Long catelogId = attrGroupEntity.getCatelogId();
        //当前分组只能关联没有被关联的属性
        //查出当前分类下其他分组 也包括自己，后面也要把自己也排除
        List<AttrGroupEntity> attrGroupOtherEntities = attrGroupDao.selectList(new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId));
        List<Long> otherGroupIds = attrGroupOtherEntities.stream().map(attrGroupEntity1 -> attrGroupEntity1.getAttrGroupId()).collect(Collectors.toList());
        //找到其他分组关联的属性
        List<AttrAttrgroupRelationEntity> groupId = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().in("attr_group_id", otherGroupIds));
        List<Long> attrIds = groupId.stream().map(g -> g.getAttrId()).collect(Collectors.toList());
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<AttrEntity>().eq("catelog_id", catelogId).eq("attr_type", ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (!CollectionUtils.isEmpty(attrIds)) {
            wrapper.notIn("attr_id", attrIds);
        }



        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            wrapper.eq("attr_id", key).or().like("attr_name", key);
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);
        PageUtils pageUtils = new PageUtils(page);
        return pageUtils;
    }

}