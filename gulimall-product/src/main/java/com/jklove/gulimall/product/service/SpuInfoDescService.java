package com.jklove.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

