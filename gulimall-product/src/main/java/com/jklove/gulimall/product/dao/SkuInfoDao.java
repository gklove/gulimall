package com.jklove.gulimall.product.dao;

import com.jklove.gulimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
