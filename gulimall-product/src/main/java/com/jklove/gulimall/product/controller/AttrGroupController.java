package com.jklove.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jklove.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.jklove.gulimall.product.entity.AttrEntity;
import com.jklove.gulimall.product.service.AttrAttrgroupRelationService;
import com.jklove.gulimall.product.service.AttrService;
import com.jklove.gulimall.product.service.CategoryService;
import com.jklove.gulimall.product.vo.AttrAttrgroupRelationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.service.AttrGroupService;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.R;



/**
 * 属性分组
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 10:37:12
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    ///product/attrgroup/attr/relation
    @RequestMapping("/attr/relation")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestBody List<AttrAttrgroupRelationVo> relationVo){
//        PageUtils page = attrGroupService.queryPage(params);
        attrGroupService.relationAttr(relationVo);
        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{categoryId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,@PathVariable("categoryId")Long categoryId){
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params,categoryId);
        return R.ok().put("page", page);
    }
    @RequestMapping("/{attrgroupId}/attr/relation")
    //@RequiresPermissions("product:attrgroup:list")
    public R attrRelation(@PathVariable("attrgroupId")Long attrgroupId){
//        PageUtils page = attrGroupService.queryPage(params);
        List<AttrEntity> list = attrService.getAttrRelation(attrgroupId);
        return R.ok().put("data", list);
    }
    @RequestMapping("/{attrgroupId}/noattr/relation")
    //@RequiresPermissions("product:attrgroup:list")
    public R noAttrRelation(@PathVariable("attrgroupId")Long attrgroupId,@RequestParam Map<String,Object> params){
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page =attrService.getNoAttrRelation(attrgroupId,params);
        return R.ok().put("page", page);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //RequiresPermissions@("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long[] catelogPath = categoryService.getcatelogPath(attrGroup.getCatelogId());
        attrGroup.setAttrGroupWithPath(catelogPath);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }
    /**
     * 移除属性关联
     */
    @RequestMapping("/attr/relation/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R attrRelationDelete(@RequestBody AttrAttrgroupRelationVo[] relationVo){
        attrGroupService.removeRelation(relationVo);

        return R.ok();
    }
}
