package com.jklove.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateWithRelation(BrandEntity brand);
}

