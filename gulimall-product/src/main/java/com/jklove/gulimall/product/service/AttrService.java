package com.jklove.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.product.entity.AttrEntity;
import com.jklove.gulimall.product.vo.AttrEntityRespVo;
import com.jklove.gulimall.product.vo.AttrEntityVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrEntityVo attr);

    PageUtils baseAttrlist(Map<String, Object> params, Long categoryId,String attrType);

    AttrEntityRespVo getAttr(Long attrId);

    void updateAttr(AttrEntityVo attr);

    List<AttrEntity> getAttrRelation(Long attrgroupId);

    PageUtils getNoAttrRelation(Long attrgroupId, Map<String, Object> params);
}

