package com.jklove.gulimall.product.entity.costume;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class AttrGroupWithPath {

    private Long[] catelogPath;

    @TableField(exist = false)
    private String catelogPathName;

}
