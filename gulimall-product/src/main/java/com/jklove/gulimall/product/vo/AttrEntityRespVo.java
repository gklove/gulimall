package com.jklove.gulimall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class AttrEntityRespVo extends AttrEntityVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //所属分类的名字
    private String catelogName;
    //所属分组的名字
    private String groupName;
}
