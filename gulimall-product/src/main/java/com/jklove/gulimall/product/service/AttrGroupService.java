package com.jklove.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jklove.common.utils.PageUtils;
import com.jklove.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.jklove.gulimall.product.entity.AttrEntity;
import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.vo.AttrAttrgroupRelationVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-20 21:53:25
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long categoryId);

    void removeRelation(AttrAttrgroupRelationVo[] relationVo);

    void relationAttr(List<AttrAttrgroupRelationVo> relationVo);

}

