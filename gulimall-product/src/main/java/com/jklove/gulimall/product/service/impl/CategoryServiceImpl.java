package com.jklove.gulimall.product.service.impl;

import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.entity.CategoryBrandRelationEntity;
import com.jklove.gulimall.product.service.CategoryBrandRelationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.Query;

import com.jklove.gulimall.product.dao.CategoryDao;
import com.jklove.gulimall.product.entity.CategoryEntity;
import com.jklove.gulimall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {


    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1.查出全部分类
        List<CategoryEntity> list = baseMapper.selectList(null);

        //2.组装全部一级分类
        List<CategoryEntity> levelMenus = list.stream()
                .filter(categoryEntity -> categoryEntity.getParentCid() == 0)
                .map((menu) -> {
                    menu.setChildren(getChildren(menu, list));
                    return menu;
                })
                .sorted((menu1,menu2) -> {
                    return (menu1.getSort()==null?0:menu1.getSort()) - (menu2.getSort()==null?0:menu2.getSort());
                })
                .collect(Collectors.toList());

        return levelMenus;
    }

    @Override
    public Long[] getcatelogPath(Long attrGroupId) {
        List<Long> path = new ArrayList<>();
        findcatelogPath(attrGroupId,path);
        Collections.reverse(path);
        return path.toArray(new Long[path.size()]);
    }

    @Override
    public void createPathName(List<AttrGroupEntity> records) {

        records.forEach(record -> {
            List<String> name = new ArrayList<>();
            this.findcatelogPathName(record.getCatelogId(),name );
            Collections.reverse(name);
            record.setPathName(StringUtils.join(name.toArray(),"/"));
        });

    }

    @Override
    public void updateWithRelation(CategoryEntity category) {
        this.updateById(category);
        //更新关联表
        if (StringUtils.isNotBlank(category.getName())) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        }
    }

    private void findcatelogPath(Long catelogId,List<Long> path ) {
        path.add(catelogId);
        CategoryEntity entity = this.getById(catelogId);
        if (entity.getParentCid() != 0) {
            findcatelogPath(entity.getParentCid(),path);
        }
    }

    private void findcatelogPathName(Long catelogId,List<String> names) {
        CategoryEntity entity = this.getById(catelogId);
        names.add(entity.getName());
        if (entity.getParentCid() != 0) {
            findcatelogPathName(entity.getParentCid(),names);
        }
    }

    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        })
                .map((menu)->{
                    menu.setChildren(getChildren(menu, all));
                    return menu;
                })
                .sorted((menu1,menu2)->{
                    return (menu1.getSort()==null?0:menu1.getSort()) - (menu2.getSort()==null?0:menu2.getSort());
                })
                .collect(Collectors.toList());

        return children;
    }


}