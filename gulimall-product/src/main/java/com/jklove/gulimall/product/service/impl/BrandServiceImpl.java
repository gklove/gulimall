package com.jklove.gulimall.product.service.impl;

import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.jklove.gulimall.product.entity.CategoryBrandRelationEntity;
import com.jklove.gulimall.product.service.CategoryBrandRelationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.Query;

import com.jklove.gulimall.product.dao.BrandDao;
import com.jklove.gulimall.product.entity.BrandEntity;
import com.jklove.gulimall.product.service.BrandService;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(key)) {
            wrapper.and((w) -> {
                w.eq("brand_id", key).or().like("name", key);

            });

        };
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void updateWithRelation(BrandEntity brand) {
        this.updateById(brand);
        if (StringUtils.isNotBlank(brand.getName())) {
            //更新关联表


            categoryBrandRelationService.updateBrand(brand.getBrandId(),brand.getName());

            //TODO 其他关联表冗余字段更新
        }
    }

}