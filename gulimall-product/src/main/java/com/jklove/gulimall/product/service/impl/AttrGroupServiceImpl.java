package com.jklove.gulimall.product.service.impl;

import com.jklove.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.jklove.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.jklove.gulimall.product.entity.AttrEntity;
import com.jklove.gulimall.product.service.AttrAttrgroupRelationService;
import com.jklove.gulimall.product.service.CategoryService;
import com.jklove.gulimall.product.vo.AttrAttrgroupRelationVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jklove.common.utils.PageUtils;
import com.jklove.common.utils.Query;

import com.jklove.gulimall.product.dao.AttrGroupDao;
import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.service.AttrGroupService;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrAttrgroupRelationDao relationDao;
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long categoryId) {
        if (categoryId == 0) {
            QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<>();
//            wrapper.eq("catelog_id", categoryId);
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), wrapper);
            List<AttrGroupEntity> records = page.getRecords();
            categoryService.createPathName(records);
            return new PageUtils(page);

        } else {
            String key = (String) params.get("key");
            QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<>();
            wrapper.eq("catelog_id", categoryId);
            if (StringUtils.isNotBlank(key)) {
                wrapper.and((obj) -> {
                    obj.eq("attr_group_id", key).or().like("attr_group_name", key);
                });
            }
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), wrapper);
            List<AttrGroupEntity> records = page.getRecords();
           categoryService.createPathName(records);

            return new PageUtils(page);
        }
    }

    @Override
    public void removeRelation(AttrAttrgroupRelationVo[] relationVo) {
        List<AttrAttrgroupRelationEntity> list = Arrays.asList(relationVo).stream().map((r) -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(r, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());
        relationDao.deleteBatchRelation(list);
    }

    @Override
    public void relationAttr(List<AttrAttrgroupRelationVo> relationVo) {
        List<AttrAttrgroupRelationEntity> list = relationVo.stream().map(r -> {
            AttrAttrgroupRelationEntity entity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(r, entity);
            return entity;
        }).collect(Collectors.toList());
        attrAttrgroupRelationService.saveBatch(list);
    }


}