package com.jklove.gulimall.product;

import com.jklove.gulimall.product.entity.AttrGroupEntity;
import com.jklove.gulimall.product.entity.BrandEntity;
import com.jklove.gulimall.product.service.BrandService;
import com.jklove.gulimall.product.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


@Slf4j
@SpringBootTest
class GulimallProductApplicationTests {

	@Autowired
	BrandService brandService;

	@Autowired
	private CategoryService categoryService;
	@Test
	void aaa() {
		Long[] longs = categoryService.getcatelogPath(225L);
		log.info("完整路径：{}",longs);
	}
	@Test
	void contextLoads() {
		BrandEntity brandEntity = new BrandEntity();
		brandEntity.setDescript("hello");
		brandEntity.setName("华为");
		brandService.save(brandEntity);
		System.out.println("保存成功");
	}

}
