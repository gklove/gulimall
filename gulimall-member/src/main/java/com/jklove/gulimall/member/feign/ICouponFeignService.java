package com.jklove.gulimall.member.feign;

import com.jklove.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("gulimall-coupon")
public interface ICouponFeignService {
    @GetMapping("/coupon/coupon/feign/test")
    public R getCouponList();

}
