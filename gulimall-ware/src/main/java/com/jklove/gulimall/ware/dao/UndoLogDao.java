package com.jklove.gulimall.ware.dao;

import com.jklove.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 11:56:47
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
