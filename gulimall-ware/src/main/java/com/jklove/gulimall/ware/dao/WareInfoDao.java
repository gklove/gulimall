package com.jklove.gulimall.ware.dao;

import com.jklove.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author xuedao
 * @email sun@qq.com
 * @date 2022-02-21 11:56:47
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
